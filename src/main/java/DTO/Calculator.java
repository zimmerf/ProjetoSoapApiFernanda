package DTO;

public class Calculator {
    String valor1Divide;
    String valor2Divide;
    String resultadoDivide;
    String valor1Multiplica;
    String valor2Multiplica;
    String resultadoMultiplica;
    String valor1Subtrai;
    String valor2Subtrai;
    String resultadoSubtrai;

    public String getValor1Divide() {
        return valor1Divide;
    }

    public void setValor1Divide(String valor1Divide) {
        this.valor1Divide = valor1Divide;
    }

    public String getValor2Divide() {
        return valor2Divide;
    }

    public void setValor2Divide(String valor2Divide) {
        this.valor2Divide = valor2Divide;
    }

    public String getResultadoDivide() {
        return resultadoDivide;
    }

    public void setResultadoDivide(String resultadoDivide) {
        this.resultadoDivide = resultadoDivide;
    }

    public String getValor1Multiplica() {
        return valor1Multiplica;
    }

    public void setValor1Multiplica(String valor1Multiplica) {
        this.valor1Multiplica = valor1Multiplica;
    }

    public String getValor2Multiplica() {
        return valor2Multiplica;
    }

    public void setValor2Multiplica(String valor2Multiplica) {
        this.valor2Multiplica = valor2Multiplica;
    }

    public String getResultadoMultiplica() {
        return resultadoMultiplica;
    }

    public void setResultadoMultiplica(String resultadoMultiplica) {
        this.resultadoMultiplica = resultadoMultiplica;
    }

    public String getValor1Subtrai() {
        return valor1Subtrai;
    }

    public void setValor1Subtrai(String valor1Subtrai) {
        this.valor1Subtrai = valor1Subtrai;
    }

    public String getValor2Subtrai() {
        return valor2Subtrai;
    }

    public void setValor2Subtrai(String valor2Subtrai) {
        this.valor2Subtrai = valor2Subtrai;
    }

    public String getResultadoSubtrai() {
        return resultadoSubtrai;
    }

    public void setResultadoSubtrai(String resultadoSubtrai) {
        this.resultadoSubtrai = resultadoSubtrai;
    }
}
