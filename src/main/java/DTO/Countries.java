package DTO;

public class Countries {
    String siglaPais;
    String capitalPais;
    String moedaPais;
    String ddiPais;

    public String getSiglaPais() {
        return siglaPais;
    }

    public void setSiglaPais(String siglaPais) {
        this.siglaPais = siglaPais;
    }

    public String getCapitalPais() {
        return capitalPais;
    }

    public void setCapitalPais(String capitalPais) {
        this.capitalPais = capitalPais;
    }

    public String getMoedaPais() {
        return moedaPais;
    }

    public void setMoedaPais(String moedaPais) {
        this.moedaPais = moedaPais;
    }

    public String getDdiPais() {
        return ddiPais;
    }

    public void setDdiPais(String ddiPais) {
        this.ddiPais = ddiPais;
    }
}
